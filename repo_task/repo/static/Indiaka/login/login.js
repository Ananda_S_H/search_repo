angular.module('login', ['ui.bootstrap', 'ui.router', 'ngAnimate']);

angular.module('login').config(function($stateProvider) {

    /* Add New States Above */

    $stateProvider.state('login', {
            url: '/login',
            templateUrl: 'login/partial/login/login.html',
            controller: 'LoginCtrl'
        })
    

});
