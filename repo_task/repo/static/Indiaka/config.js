angular.module('appConfig', [])
    .constant('config', {
        'webService': {
            'scheme': 'http',
            'host': '127.0.0.1',
            'port': '8000',
            'app': 'Indiaka'
        },
        baseUrl: 'http://127.0.0.1:8000'
    })
