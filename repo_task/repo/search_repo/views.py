from django.shortcuts import render

# Create your views here.

from rest_framework.decorators import api_view
from rest_framework.response import Response
import urllib2
import json



@api_view(['POST'])
def search(request):
    url = request.data['url']
    print url

    req = urllib2.Request(url='https://api.github.com/search/repositories?q='+url+'&sort=stars&order=desc')
    f = urllib2.urlopen(req)
    a = f.read()
    d = json.loads(a)
    return Response({"data":d['items']})
