from __future__ import unicode_literals

from django.apps import AppConfig


class SearchRepoConfig(AppConfig):
    name = 'search_repo'
